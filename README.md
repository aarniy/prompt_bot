# prompt_bot

## Discord bot for interesting drawing prompts.

To be used in a dedicated channel on an art server. The bot posts a prompt or set of prompts everyday, and users will make art based on those.

## To come:

- Make the daily prompt less nonsensical:
  - A list of meaningful nouns to be used, instead of pulling from an external library which contains unusable ones
  - Make sure that combinations of nouns on the list can't be interpreted as offensive
- One daily google street view -reference image everyday!
  - Setup a script that gets a completely random street view location
