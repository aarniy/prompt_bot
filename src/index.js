const Discord = require("discord.js");
const Sentencer = require("sentencer");
//const BernisClient = require("./Structures/BernisClient");
const config = require("../config.json");

const forms = ["Yooooo look, {{ a_noun }}, {{ a_noun }} and {{ a_noun }} bro"];

const client = new Discord.Client();

const dayInMilliseconds = 1000 * 60 * 60 * 24;
let whetherDailyPostIsAllowed = 0;
let dailyChannel = null;

client.once("ready", () => {
  console.log("ready to roll bro");
  dailyprompt();
});

function dailyprompt() {
  setInterval(function () {
    if (whetherDailyPostIsAllowed === 1 && dailyChannel != null) {
      dailyChannel.send(Sentencer.make("The mood is {{ adjective }} bro..."));
      dailyChannel.send(
        Sentencer.make(forms[Math.floor(Math.random() * forms.length)])
      );
    } else {
      return;
    }
  }, dailyChannel);
  return;
}

client.on("message", (message) => {
  if (message.content === "what's that bro") {
    message.channel.send(
      Sentencer.make(forms[Math.floor(Math.random() * forms.length)])
    );
  } else if (message.content === "how goes it bro") {
    message.channel.send(Sentencer.make("The mood is {{ adjective }} bro"));
  } else if (
    message.content === "bro wake up" &&
    message.member.roles.cache.some((role) => role.name === "a")
  ) {
    whetherDailyPostIsAllowed = 1;
    message.channel.send("Now sending daily prompts bro");
    dailyChannel = message.channel;
  } else if (
    message.content === "bro stfu" &&
    message.member.roles.cache.some((role) => role.name === "a")
  ) {
    message.channel.send("ok ok bro I'll stop lol");
    whetherDailyPostIsAllowed = 0;
  }
});

client.login(config.token);
